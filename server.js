const express = require("express");
const path = require("path");

const app = express();

app.use("/src", express.static(path.resolve(__dirname, "src")));

/* Redirect all routes to our (soon to exist) "index.html" file */
app.get("/*", (req, res) => {
    res.sendFile(path.resolve( "index.html"));
});

app.listen(process.env.PORT || 3000, () => console.log(`Server running... at Port ${process.env.PORT || 3000}`));
