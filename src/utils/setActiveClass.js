export function setActiveClass(className) {
    Array.from(document.getElementsByClassName(className)).map(elem => {
        if(elem.pathname === location.pathname) {
            elem.classList.add("active");
        } else {
            elem.classList.remove("active");
        }

    })
}
