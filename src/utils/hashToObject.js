export function hashToObject() {
        let searchObject = location.hash.substring(1).split('/')
        return {
            page: searchObject[1]||1,
            name: searchObject[0],
        };
}
