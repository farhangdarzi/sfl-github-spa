import AbstractView from "/src/pages/AbstractView.js";
import {globals} from "/src/utils/globals.js";
import {repositoryList} from "/src/components/repository-list.js";
import {navigationComponent} from "/src/components/navigation.js";
import {navigateTo} from "/src/packages/router.js";
import {hashToObject} from "/src/utils/hashToObject.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.id = params.id
        this.setTitle("User");
        this.repositoryListCmp = new repositoryList()
        this.navigationCmp = new navigationComponent()
    }

    async getUser() {

        return fetch(`${globals.baseUrl}/user/${this.id}`).then(res => res.json())
    }

    async getRepositories(url) {
        return fetch(url+`?page=${hashToObject().page}`).then(res => res.json())
    }

    pushState(searchObject) {
        location.hash = `#${searchObject[0]}/${searchObject[1]}`
        history.pushState(undefined, undefined, `#${searchObject[0]}/${searchObject[1]}`)
    }

    onViewInit() {
        document.querySelector('.navigation--back').addEventListener('click', () => {
            navigateTo('/')
        })

        this.navigationCmp.onViewInit()
    }

    async getHtml() {
        const user = await this.getUser()
        const repositories = {
            items: await this.getRepositories(user.repos_url)
        }


        return `
            <main>
        <div class="page__top">
            <div class="navigation navigation--back" >
                <div class="navigation__item">
                    <svg version="1.1" width="3em"  id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve">
                    <path style="fill:#4fa2a4" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
                        c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
                        c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
                    </svg>
                    <span>Back to users</span>
                </div>
            </div>
        </div>
        <div class="user-list">
            <div class="card">
                <div class="card__content">
                    <div class="card__image-wrapper">
                        <img class="card__image" src="${user.avatar_url}" alt="">
                    </div>
                    <div class="card__title">
                        ${user.login}
                    </div>
                </div>
            </div>
            <div class="user__description">
                <ul>
                    <li>
                        UserName : ${user.login}
                    </li>
                </ul>
            </div>
        </div>
        ${this.navigationCmp.getHtml()}
        <div class="repository-list__title">
            User's repositories
        </div>
        <div class="repository-list">
           ${this.repositoryListCmp.getHtml(repositories)}
        </div>
        ${this.navigationCmp.getHtml()}
    </main>
        `;
    }
}
