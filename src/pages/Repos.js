import AbstractView from "./AbstractView.js";
import {hashToObject} from "/src/utils/hashToObject.js";
import {globals} from "/src/utils/globals.js";
import {navigationComponent} from "/src/components/navigation.js";
import {repositoryList} from "/src/components/repository-list.js";
import {searchBoxInput} from "/src/components/searchBoxInput.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.postId = params.id;
        this.setTitle("Repositories");
        this.navigationCmp = new navigationComponent()
        this.repositoryListCmp = new repositoryList()
        this.searchBoxCmp = new searchBoxInput()
    }

    onViewInit() {
        this.navigationCmp.onViewInit()
        this.searchBoxCmp.onViewInit()
    }

    async getRepos(searchObject) {
        let query = encodeURIComponent(`${searchObject.name} in:name`);
        return fetch(`${globals.baseUrl}/search/repositories?q=${query}&page=${searchObject.page}&per_page=20`)
          .then((res) => {
              return res.json()
          })
    }

    async getHtml() {
        const repositories = await this.getRepos(hashToObject());

        const isFindRepo = repositories.total_count && repositories.total_count !== 0;

        return `
        <main>
            ${this.searchBoxCmp.getHtml('repos')}
            ${isFindRepo ? this.navigationCmp.getHtml():''}
            <div class="repository-list">
                 ${this.repositoryListCmp.getHtml(repositories)}
            </div>
            ${isFindRepo ? this.navigationCmp.getHtml():''}
        </main>
        `;
    }
}
