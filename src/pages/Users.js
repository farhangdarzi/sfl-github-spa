import AbstractView from "/src/pages/AbstractView.js";
import {navigationComponent} from "/src/components/navigation.js";
import {hashToObject} from "/src/utils/hashToObject.js";
import {globals} from "/src/utils/globals.js";
import {searchBoxInput} from "/src/components/searchBoxInput.js";
import {usersList} from "/src/components/usersList.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Users");
        this.navigationCmp = new navigationComponent()
        this.searchBoxCmp = new searchBoxInput()
        this.usersListCmp = new usersList()
    }


    async getUsers(searchObject) {
        let query = encodeURIComponent(`${searchObject.name} in:name`);
        return fetch(`${globals.baseUrl}/search/users?q=${query}&page=${searchObject.page}&per_page=20`)
          .then((res) => {
              return res.json()
          })
    }

    onViewInit() {
        this.navigationCmp.onViewInit()
        this.searchBoxCmp.onViewInit()
        this.usersListCmp.onViewInit()
    }

    async getHtml() {
        const users = await this.getUsers(hashToObject());
        const isFindUser = users.total_count && users.total_count !== 0;


        return `
            <main">
                ${this.searchBoxCmp.getHtml('users')} 
                ${isFindUser ? this.navigationCmp.getHtml() : ''}
                <div class="user-list">
                    ${this.usersListCmp.getHtml(users)}
                </div>
                ${isFindUser ? this.navigationCmp.getHtml() : ''}
            </main>
        `;
    }
}
