export class repositoryList {
    constructor() {

    }

    onViewInit() {

    }

    getHtml(repositories) {
        if(!repositories.items) return ''

        if (repositories.total_count === '') {
            return 'no repository item'
        }
        return repositories.items.reduce((acc, repo) => {
            acc += `<div class="repository-list__item">
                       ${repo.full_name}
                    </div>`
            return acc
        }, ``)
    }
}
