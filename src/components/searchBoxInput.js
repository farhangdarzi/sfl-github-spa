import {hashToObject} from "/src/utils/hashToObject.js";

export class searchBoxInput {
    constructor() {

    }

    pushState(searchObject) {
        location.hash = `#${searchObject[0]}/${searchObject[1]}`
        history.pushState(undefined, undefined, `#${searchObject[0]}/${searchObject[1]}`)
    }

    onViewInit() {
        let searchObject = location.hash.substring(1).split('/');
        document.querySelector('.search-box__input').value = hashToObject().name || ''
        document.querySelector('.search-box__input').addEventListener('keyup', async (event) => {
            searchObject[0] = event.target.value;
            searchObject[1] = '1';
            if (event.key === "Enter") {
                this.pushState(searchObject);
            }
        })
    }

    getHtml(placeholder = '...') {
        return `<input type="text" class="search-box__input" placeholder="Search for ${placeholder}">`
    }
}
