import {navigateTo} from "/src/packages/router.js";

export class usersList {
    constructor() {

    }

    onViewInit() {
        document.querySelectorAll('.card').forEach((curr) => {
            curr.addEventListener('click', () => {
                navigateTo(`/user/${curr.dataset.id}#/1`)
            })
        })
    }

    getHtml(users) {
        if (!users.items) {
            return ''
        }
        if (users.total_count === 0) {
            return '<div>No user items</div>'
        }

        return users.items.reduce((acc, user) => {
            acc += `<a class="card" data-id="${user.id}">
                        <div class="card__content" >
                            <div class="card__image-wrapper">
                                <img class="card__image" src="${user.avatar_url}" alt="">
                            </div>
                            <div class="card__title">
                                ${user.login}
                            </div>
                        </div>
                    </a>`

            return acc
        }, ``)
    }
}
