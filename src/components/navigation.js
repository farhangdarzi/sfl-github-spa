export class navigationComponent {
    constructor() {
        this.searchObject = location.hash.substring(1).split('/');
    }

    pushState() {
        this.searchObject[1]=this.searchObject[1] || 1
        location.hash = `#${this.searchObject[0]}/${this.searchObject[1]}`
        history.pushState(undefined, undefined, `#${this.searchObject[0]}/${this.searchObject[1]}`)
    }

    handleOnClickNext() {
        document.querySelectorAll('.navigation__item--next').forEach((curr) => {
            curr.addEventListener('click', () => {
                this.searchObject[1]++;
                this.pushState();
            })
        })
    }

    handleOnClickPrev() {
        document.querySelectorAll('.navigation__item--prev').forEach((curr) => {
            curr.addEventListener('click', () => {
                if (this.searchObject[1] > 0) {
                    this.searchObject[1]--;
                    this.pushState();
                }
            })
        })

    }

    onViewInit() {
        this.handleOnClickPrev()
        this.handleOnClickNext()
    }

    getHtml() {
        return `
            <div class="navigation--wrapper">
              <div class="navigation">
                  <div class="navigation__item navigation__item--prev">
                    <svg version="1.1" width="3em"  id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve">
                    <path style="fill:#4fa2a4" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
                        c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
                        c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
                    </svg>
                      <span>Prev</span>
                  </div>
                  <div class="navigation__item navigation__item--next">
                      <span>Next</span>
                    <svg version="1.1" width="3em" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve">
                    <path style="fill:#4fa2a4" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                        C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                        c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                    </svg>
                  </div>
              </div>
           </div>
    `
    }
}
