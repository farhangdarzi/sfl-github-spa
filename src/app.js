import {router,navigateTo} from "/src/packages/router.js";


window.addEventListener("popstate", router);

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", e => {
        e.preventDefault()
        if (e.target.matches("[data-link]")) {
            e.preventDefault();
            navigateTo(e.target.href);
        }
    });

    navigateTo(location.pathname+location.hash);
});
