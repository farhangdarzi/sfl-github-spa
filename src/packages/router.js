import Users from "/src/pages/Users.js";
import User from "/src/pages/User.js";
import Repos from "/src/pages/Repos.js";
import {setActiveClass} from "/src/utils/setActiveClass.js";

const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");
const getParams = match => {
    const values = match.result.slice(1);
    const keys = Array.from(match.route.path.matchAll(/:(\w+)/g)).map(result => result[1]);

    return Object.fromEntries(keys.map((key, i) => {
        return [key, values[i]];
    }));
};

export const router = async () => {
    const routes = [
        {name: 'users', path: "/", view: Users},
        {name: 'user', path: "/user/:id", view: User},
        {name: 'repos', path: "/repos", view: Repos},
    ];

    const potentialMatches = routes.map(route => {
        return {
            route: route,
            result: location.pathname.match(pathToRegex(route.path))
        };
    });

    let match = potentialMatches.find(potentialMatch => potentialMatch.result !== null);

    if (!match) {
        match = {
            route: routes[0],
            result: [location.pathname]
        };
    }

    const view = new match.route.view(getParams(match));
    document.querySelector("#app").innerHTML = 'Loading ...';
    document.querySelector("#app").innerHTML = await view.getHtml();
    view.onViewInit()
    setActiveClass('app__title-link');
};

export  function navigateTo(url) {
    history.pushState(null, null, url);
    router();
}
